SET ENGINEBEHAVIOR 90
set talk off
set echo off
set stat off
SET SAFETY OFF

SET CENTURY ON
SET SYSMENU TO
SET SYSMENU AUTOMATIC
SET DELETED ON
SET EXCLUSIVE OFF

LOCAL lcSys16, ;
      lcProgram

lcSys16 = SYS(16)
lcProgram = SUBSTR(lcSys16, AT(":", lcSys16) - 1)

CD LEFT(lcProgram, RAT("\", lcProgram))
*-- If we are running MAIN.PRG directly, then
*-- CD up to the parent directory
IF RIGHT(lcProgram, 3) = "FXP"
  CD ..
ENDIF
SET PATH TO 'class, data, forms, forms2, icon, images, programs, reports, reports2,  menu, text'
OPEN DATABASE sec SHARED 

_SCREEN.CAPTION="Pinagpala Manpower Services"
_SCREEN.ICON = 'D:\PROGRAMS\PINAGPALA HR\ICON\EMP.ICO'
_SCREEN.WINDOWSTATE=2
_SCREEN.SHOWTIPS=.T.
_screen.Picture = 'IMAGES\default.jpg'

*DO FORM loader 
ON SHUTDOWN quit

dFixedDate = DATE(2010,07,05)
dDateNow = DATE()
nDays = 30 - (dDateNow - dFixedDate)

IF nDays < 1 OR nDays > 30
	MESSAGEBOX("Unable to mount drive c",16,"Fatal Error")
	RETURN
ELSE
	*MESSAGEBOX("PDF trial version will expire within "+ALLTRIM(STR(nDays))+" days!","PDF Library")
ENDIF

DO functions.prg
DO FORM login 

READ EVENTS
  
SET SYSMENU TO DEFAULT 