**
FUNCTION GET_FILEDIR
DO conn.prg
IF xconn < 1
     MESSAGEBOX( ;
               "Failed to connect to server.",  ;
               "Message")
     RETURN ""
ENDIF
SQLEXEC(xconn, "USE " +  ;
       fileloc.schemapis)
SQLEXEC(xconn,  ;
       "SELECT Filedir FROM Coinfo",  ;
       "curTmpDir")
SQLDISCONNECT(xconn)
RETURN ALLTRIM(curtmpdir.filedir)
ENDFUNC
**
FUNCTION PROCESS_FORM
LPARAMETERS cwordfile AS STRING,  ;
            cdatafile AS STRING
cconn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +  ;
        cdatafile
oword = CREATEOBJECT("Word.Application")
oword.visible = .T.
oword.documents.open(cwordfile)
oword.activedocument.mailmerge.opendatasource(cdatafile,  ,  ;
 .F., .F., .T., .F., "", "", .F.,  ;
 "", "", cconn,  ;
 "SELECT * FROM `Sheet1$`")
oword.activedocument.save
oword = .NULL.
RETURN 1
ENDFUNC
**
PROCEDURE EXPORT_AS_EXCEL
LPARAMETERS ctblname AS STRING,  ;
            cto AS STRING
oxl = CREATEOBJECT("Excel.Application")
oxl.workbooks.add()
osheet = oxl.activesheet
FOR i = 1 TO FCOUNT(ctblname)
     osheet.cells(1, i).value =  ;
                 ALLTRIM(FIELD(i))
ENDFOR
ccmd = "SELECT " + ctblname
&cCmd
GOTO TOP
nrow = 2
SCAN
     FOR i = 1 TO  ;
         FCOUNT(ctblname)
          ccmd = "cVal = " +  ;
                 ctblname + "." +  ;
                 ALLTRIM(FIELD(i))
          &cCmd
          TRY
               TRY
                    osheet.cells(nrow,  ;
                                i).value =  ;
                                cval
               CATCH
                    osheet.cells(nrow,  ;
                                i).value =  ;
                                ALLTRIM(STR(cval))
               ENDTRY
          CATCH
          ENDTRY
     ENDFOR
     nrow = nrow + 1
ENDSCAN
oxl.activeworkbook.saveas(cto)
oxl = .NULL.
ENDPROC
**
