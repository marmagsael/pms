SET ENGINEBEHAVIOR 90
SET TALK OFF
SET ECHO OFF
SET STATUS OFF
SET SAFETY OFF
SET CENTURY ON
SET SYSMENU TO
SET SYSMENU AUTOMATIC
SET DELETED ON
SET EXCLUSIVE OFF
LOCAL lcsys16, lcprogram
lcsys16 = SYS(16)
lcprogram = SUBSTR(lcsys16,  ;
            AT(":", lcsys16) -  ;
            1)
CD LEFT(lcprogram, RAT("\",  ;
   lcprogram))
IF RIGHT(lcprogram, 3) = "FXP"
     CD ..
ENDIF
SET PATH TO 'class, data, forms, forms2, icon, images, programs, reports, reports2,  menu, text'
OPEN DATABASE sec SHARED
_SCREEN.caption = "Pinagpala Manpower Services"
_SCREEN.icon = 'D:\PROGRAMS\PINAGPALA HR\ICON\EMP.ICO'
_SCREEN.windowstate = 2
_SCREEN.showtips = .T.
_SCREEN.picture = 'IMAGES\default.jpg'
ON SHUTDOWN QUIT
DO functions.prg
DO FORM login
READ EVENTS
SET SYSMENU TO DEFAULT
ENDPROC
