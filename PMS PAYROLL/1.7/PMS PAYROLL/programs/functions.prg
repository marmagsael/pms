FUNCTION getServerDate() as Date
	DO conn.prg
	IF xconn < 1
		MESSAGEBOX("Failed to connect to server","Message")
		RETURN null
	ENDIF
	
	SQLEXEC(xconn,"Select Now() sDate","curServerDate")
	SQLDISCONNECT(xconn)
	
	RETURN curServerDate.sDate
ENDFUNC