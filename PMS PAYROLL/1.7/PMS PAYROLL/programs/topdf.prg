FUNCTION ToPDF
	PARAMETERS cReportName as String, cFilter as String
*!*		dFixedDate = DATE(2010,06,13)
*!*		dDateNow = DATE()
*!*		nDays = 30 - (dDateNow - dFixedDate)
*!*		
*!*		IF nDays < 1 OR nDays > 30
*!*			MESSAGEBOX("PDF trial version has expired!","PDF Library")
*!*			RETURN
*!*		ELSE
*!*			MESSAGEBOX("PDF trial version will expire within "+ALLTRIM(STR(nDays))+" days!","PDF Library")
*!*		ENDIF
	
	TRY
		IF cFilter == .f.
			cFilter = ""
		ENDIF
	CATCH
	ENDTRY
	
	SET PROCEDURE TO utilityReportListener.prg ADDITIVE

	local loXfrx, lnRetval 
	loxfrx= xfrx("XFRX#LISTENER") 
	lnRetVal = loxfrx.SetParams("output.pdf",,.t.,,,,"PDF") 
	
	If lnRetVal = 0
		cCmd = "REPORT FORM "+cReportName+ " " +cFilter+ "OBJECT loxfrx"
		&cCmd
		RUN ("C:\Program Files\Foxit Software\Foxit Reader\Foxit Reader.exe" "output.pdf")
		
*!*			cCmd = "REPORT FORM "+cReportName+ " " +cFilter+ "to printer prompt nodialog preview"
*!*			&cCmd
	Else 
	    MESSAGEBOX("An error has occurred.","Error") 
	ENDIF
ENDFUNC