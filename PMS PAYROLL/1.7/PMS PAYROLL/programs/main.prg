SET ENGINEBEHAVIOR 70
PUBLIC xpay, xpis 

SET talk OFF 
SET echo OFF 
SET STATUS OFF 
SET SAFETY OFF

SET CENTURY ON
SET SYSMENU TO
SET DELETED ON
SET EXCLUSIVE OFF

SET DECIMALS TO 2

LOCAL lcSys16, lcProgram

lcSys16 	= SYS(16)
lcProgram 	= SUBSTR(lcSys16, AT(":", lcSys16) - 1)

CD LEFT(lcProgram, RAT("\", lcProgram))
*-- If we are running MAIN.PRG directly, then 
*-- CD up to the parent directory
IF RIGHT(lcProgram, 3) = "FXP"
  CD ..
ENDIF
SET PATH TO 'class, data, forms, programs, reports, menu, text, images' ADDITIVE
OPEN DATABASE sec SHARED 

_SCREEN.CAPTION="PAYROLL SYSTEM - PINAGPALA MANPOWER SERVICES"
_SCREEN.ICON= ''
_SCREEN.WINDOWSTATE=2
_SCREEN.SHOWTIPS=.T.
_Screen.Closable = .t.
_screen.BackColor = RGB(128,128,128)

ON SHUTDOWN quit

PUBLIC MM_USERNAME
MM_USERNAME = ""

SELECT * FROM fileloc INTO CURSOR x 
xpis 	= ALLTRIM(x.schemapis)
xpay	= ALLTRIM(x.schemapay)


DO functions.prg
DO FORM frmlogin.scx

READ EVENTS
 
SET SYSMENU TO DEFAULT