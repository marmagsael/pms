IIF(ALLTRIM("Daysx") == "Days",  ;
   IIF(RIGHT(ALLTRIM(STR(curwsfin.col1,  ;
   12, 2)), 2) == "00",  ;
   LEFT(ALLTRIM(STR(curwsfin.col1,  ;
   12, 2)),  ;
   LEN(ALLTRIM(STR(curwsfin.col1,  ;
   12, 2))) - 3),  ;
   IIF(RIGHT(ALLTRIM(STR(curwsfin.col1,  ;
   12, 2)), 1) == "0",  ;
   LEN(ALLTRIM(STR(curwsfin.col1,  ;
   12, 2))) - 1,  ;
   ALLTRIM(STR(curwsfin.col1, 12,  ;
   2)))), curwsfin.col1)
ENDPROC
**
