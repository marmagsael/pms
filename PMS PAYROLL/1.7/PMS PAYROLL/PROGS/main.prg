*** 
*** ReFox X  #UK933629  MANRIQUE ORELLANA  MANSOFT SYSTEMS [VFP80]
***
SET ENGINEBEHAVIOR 70
PUBLIC xpay, xpis
SET TALK OFF
SET ECHO OFF
SET STATUS OFF
SET SAFETY OFF
SET CENTURY ON
SET SYSMENU TO
SET DELETED ON
SET EXCLUSIVE OFF
SET DECIMALS TO 2
LOCAL lcsys16, lcprogram
lcsys16 = SYS(16)
lcprogram = SUBSTR(lcsys16,  ;
            AT(":", lcsys16) -  ;
            1)
CD LEFT(lcprogram, RAT("\",  ;
   lcprogram))
IF RIGHT(lcprogram, 3) = "FXP"
     CD ..
ENDIF
SET PATH TO 'class, data, forms, programs, reports, menu, text, images';
ADDITIVE
OPEN DATABASE sec SHARED
_SCREEN.caption = "PAYROLL SYSTEM - PINAGPALA MANPOWER SERVICES"
_SCREEN.icon = ''
_SCREEN.windowstate = 2
_SCREEN.showtips = .T.
_SCREEN.closable = .T.
_SCREEN.backcolor = RGB(128, 128,  ;
                    128)
ON SHUTDOWN QUIT
PUBLIC mm_username
mm_username = ""
SELECT * FROM fileloc INTO CURSOR  ;
         x
xpis = ALLTRIM(x.schemapis)
xpay = ALLTRIM(x.schemapay)
DO functions.prg
DO FORM frmlogin.scx
READ EVENTS
SET SYSMENU TO DEFAULT
ENDPROC
**
*** 
*** ReFox - retrace your steps ... 
***
