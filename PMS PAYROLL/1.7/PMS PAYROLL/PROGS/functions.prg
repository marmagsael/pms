**
FUNCTION getServerDate
DO conn.prg
IF xconn < 1
     MESSAGEBOX( ;
               "Failed to connect to server",  ;
               "Message")
     RETURN .NULL.
ENDIF
SQLEXEC(xconn,  ;
       "Select Now() sDate",  ;
       "curServerDate")
SQLDISCONNECT(xconn)
RETURN curserverdate.sdate
ENDFUNC
**
